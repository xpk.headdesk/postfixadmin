# Ansible playbook: install postfixadmin, dovecot, and rainloop webmail
## URL: https://gitlab.com/xpk.headdesk/postfixadmin

## Prerequisites:
- This playbook was build on CentOS 7
- EPEL and IUS repo are used in this playbook.

## Roles
- base-packages: installs apache, php, mysql. If you already have these, skip this role
- pfa-dovecot: installs pfa, dovecot, and rainloop webmail

After playbook is ran, continue with pfa setup at http://your-server/postfixadmin/setup.php 
Setup password is pfa-setup-2018, which is in $CONF['setup_password'] of config.local.php

Setup rainloop webmail by going to http://your-server/webmail/?admin. Default setup password is admin/12345
Delete all pre-configured domains and add yours. In the connection settings, enter 127.0.0.1 for incoming and outgoing. Change the setup password.

## TODO
After this playbook is ran, the following should be performed before the mail server is open for service
* Setup https on pfa and rainloop
* Setup ssl on dovecot
* Implement relay control with SPF / RBL / postscreen / etc

# References
* http://postfixadmin.sourceforge.net/
* https://wiki2.dovecot.org/Variables
